#include <iostream>
#include <conio.h>
#include "Helper.h"
#include <windows.h>
using namespace std;
typedef int(__stdcall *f_funci)();

std::wstring s2ws(const std::string& s);
void createfile(string name);
void openexe(string path);
void GetExeFileName();
void changeDir(string path);
void getAllFilesFromDir();
void secret();
int main(int argc, char **argv)
{
	vector<string>cmdLine;
	string cmd;
	while (1)
	{
		cout << ">>";
		getline(cin,cmd);
		if (cmd != "")
		{
			Helper::trim(cmd);
			cmdLine = Helper::get_words(cmd);
		}
		if (cmd =="exit")
		{
			break;
		}
		else if (cmd == "pwd")
		{
			GetExeFileName();
		}
		else if (cmdLine[0] == "cd")
		{
			cmd.erase(0, 2);
			Helper::trim(cmd);
			changeDir(cmd);
		}
		else if (cmdLine[0] == "create")
		{
			createfile(cmdLine[1]);
		}
		else if (cmdLine[0] == "ls")
		{
			getAllFilesFromDir();
		}
		else if (cmd == "secret")
		{
			secret();
		}
		else if (cmdLine[0].find(".exe") != std::string::npos)
		{
			openexe(cmdLine[0]);
		}
	}
	


	
	return 0;
}
void openexe(string path)
{
	wstring wpath = s2ws(path);

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	const WCHAR * wcpath = wpath.c_str();

	// set the size of the structures
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// start the program up
	CreateProcess(wcpath,   // the path
		L"",        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi             // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
		);
	// Close process and thread handles.
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}
void secret()
{
	HINSTANCE hGetProcIDDLL = LoadLibrary(L"Secret.dll");

	if (!hGetProcIDDLL) {
		std::cout << "could not load the dynamic library" << std::endl;
	}
	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	if (!funci) {
		std::cout << "could not locate the function" << std::endl;
	}
	cout << funci() << std::endl;

}
void getAllFilesFromDir()
{
	int i;
	WCHAR path[2048];
	GetCurrentDirectory(2048, path);
	HANDLE hFind;
	WIN32_FIND_DATA data;
	for (i = 0; path[i] != 0;i++)
	{ }
	path[i] = '\\';
	i++;
	path[i] = '*';
	i++;
	path[i] = 0;
	hFind = FindFirstFile(path, &data);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			wcout << data.cFileName<<"\n";
		} while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}

}
void createfile(string name)
{
	wstring wname = s2ws(name);
	HANDLE hfile = CreateFile(
		wname.c_str(),
		GENERIC_READ,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	if (hfile == INVALID_HANDLE_VALUE)
	{
		cout << GetLastError();
	}
	CloseHandle(hfile);
}
void changeDir(string path)
{
	int erorr;
	wstring npath = s2ws(path);
	erorr = SetCurrentDirectory(npath.c_str());
	if (erorr == 0)
	{
		cout << GetLastError();
	}
}
void GetExeFileName()
{
	/*int i = 0,index;
	string s= "windows_shell.exe";
*/
	WCHAR path[2048];
	GetCurrentDirectory(2048, path);
	/*GetModuleFileNameW(hModule, path, MAX_PATH);
	for (i = 0; 0 !=path[i]; i++)
	{
		if (path[i] == 'w')
		{
			index = i;
			int k = i;
			int j;
			for (j = 0; j < s.length(); j++)
			{
				if (path[k] != s[j])
				{
					j = 10000;
				}
				k++;
			}
			if (j == s.length())
			{
				path[index] = 0;
			}
		}
	}*/
	wcout <<path << "\n";


}
std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}